import java.util.Scanner;

public class Rownanie {
	private int n;
	private double a;
	private double h;
	private double[] yEuler;
	private double[] bladEuler;
	private double[] yHeun;
	private double[] bladHeun;
	private double[] xk;
	private double[] yDokladne;

	Scanner reader = new Scanner(System.in);

	public Rownanie() {
		do {
			System.out.print("Podaj n: ");
			n = reader.nextInt();
		} while (n <= 1);
		do {
			System.out.print("Podaj a: ");
			a = reader.nextDouble();
		} while (a <= 1);
		h = (a - 1) / n;
		xk = new double[n + 1];
		yEuler = new double[n + 1];
		bladEuler = new double[n + 1];
		yHeun = new double[n + 1];
		bladHeun = new double[n + 1];
		yDokladne = new double[n + 1];
		yEuler[0] = yHeun[0] = 0;
		for (int i = 0; i <= n; i++) {
			xk[i] = xk(i);
			yDokladne[i] = yDokladne(xk[i]);
		}
		this.licz();
		this.wypiszTablice();
		System.out.println("Blad maksymalny metody Eulera wynosi: " + bladMaksymalny(bladEuler));
		System.out.println("Blad maksymalny metody Heuna wynosi: " + bladMaksymalny(bladHeun));
	}

	private double xk(int k) {
		return 1 + k * h;
	}

	private double yDokladne(double x) {
		return x * x - x;
	}

	private double f(double x, double y) {
		return Math.sqrt(y + x) + x - 1;
	}

	private double Euler(int k) {
		double wynik = yEuler[k] + h * f(xk[k], yEuler[k]);
		yEuler[k + 1] = wynik;
		return wynik;
	}

	private double Heun(int k) {
		// double wynik = yHeun[k] + h*(f(xk[k],yHeun[k]) +
		// f(xk[k]+(h/2),yHeun[k]+(h/2)*f(xk[k],yHeun[k])));
		double wynik = yHeun[k] + h * f(xk[k] + (h / 2), yHeun[k] + (h / 2) * f(xk[k], yHeun[k]));
		yHeun[k + 1] = wynik;
		return wynik;
	}

	private double blad(double yk, double yxk) {
		return Math.abs(yk - yxk);
	}

	private double bladMaksymalny(double[] blad) {
		double max = 0;
		for (int i = 0; i < blad.length; i++) {
			if (blad[i] > max)
				max = blad[i];
		}
		return max;
	}

	public void licz() {
		for (int i = 0; i < n; i++) {
			Euler(i);
			Heun(i);
		}
		for (int i = 1; i <= n; i++) {
			bladEuler[i] = blad(yEuler[i], yDokladne[i]);
			bladHeun[i] = blad(yHeun[i], yDokladne[i]);
		}
	}

	public void wypiszTablice() {
		if (n > 10) {
			double coKtory = n / 10;
			System.out.printf("\nx            ||");
			for (int i = 0; i <= n; i++) {
				if((i%coKtory==0)||(i==n)) System.out.printf(" %7.2f |", xk[i]);
			}
			System.out.println();
			System.out.printf("y(Dokladne)  ||");
			for (int i = 0; i <= n; i++) {
				if((i%coKtory==0)||(i==n)) System.out.printf(" %7.2f |", yDokladne[i]);
			}
			System.out.println();
			System.out.printf("y(Euler)     ||");
			for (int i = 0; i <= n; i++) {
				if((i%coKtory==0)||(i==n)) System.out.printf(" %7.2f |", yEuler[i]);
			}
			System.out.println();
			System.out.printf("y(Heun)      ||");
			for (int i = 0; i <= n; i++) {
				if((i%coKtory==0)||(i==n)) System.out.printf(" %7.2f |", yHeun[i]);
			}
			System.out.println();
			System.out.println("\n");
		}
		else{
			System.out.printf("\nx            ||");
			for (int i = 0; i <= n; i++) {
				System.out.printf(" %7.2f |", xk[i]);
			}
			System.out.println();
			System.out.printf("y(Dokladne)  ||");
			for (int i = 0; i <= n; i++) {
				System.out.printf(" %7.2f |", yDokladne[i]);
			}
			System.out.println();
			System.out.printf("y(Euler)     ||");
			for (int i = 0; i <= n; i++) {
				System.out.printf(" %7.2f |", yEuler[i]);
			}
			System.out.println();
			System.out.printf("y(Heun)      ||");
			for (int i = 0; i <= n; i++) {
				System.out.printf(" %7.2f |", yHeun[i]);
			}
			System.out.println();
			System.out.println("\n");
		}
	}

	public static void main(String[] args) {
		new Rownanie();
	}
}
